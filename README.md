# Shared Secret
####*Stop sharing your secrets on Slack* 🤦‍

Simple web application to safely share secrets online. Secrets are stored for an hour or until it is viewed once.
