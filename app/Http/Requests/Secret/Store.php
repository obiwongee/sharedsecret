<?php

namespace App\Http\Requests\Secret;

use Illuminate\Foundation\Http\FormRequest;

class Store extends FormRequest
{
    public function getPayload(): string
    {
        return $this->input('payload');
    }

    public function rules(): array
    {
        return [
            'payload' => 'required|string|max:5000',
        ];
    }
}
