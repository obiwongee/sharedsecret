<?php

namespace App\Http\Controllers;

use Illuminate\View\View;
use App\Models\IdGenerator;
use Illuminate\Http\Response;
use App\Http\Requests\Secret\Store;
use Illuminate\Support\Facades\Cache;
use Jenssegers\Agent\Agent;

class SecretController extends Controller
{
    public function store(Store $request, IdGenerator $generator): Response
    {
        $key = $generator->generateId();
        $expiresAt = now()->addHour();
        Cache::put($key, encrypt($request->getPayload()), $expiresAt);

        $response = [
            'id' => $key,
            'url' => config('app.url') . '/s/' . $key,
            'expires_at' => $expiresAt->format('Y/m/d h:i:s a') . ' UTC',
        ];

        return response($response, Response::HTTP_CREATED);
    }

    public function show(string $id, Agent $agent): View
    {
        if ($agent->isRobot()) {
            abort(Response::HTTP_NOT_FOUND);
        }

        $payload = Cache::get($id);

        if (!$payload) {
            abort(Response::HTTP_NOT_FOUND);
        }

        Cache::forget($id);

        return view('secret', [
            'secret' => decrypt($payload),
        ]);
    }
}
