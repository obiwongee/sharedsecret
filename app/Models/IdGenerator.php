<?php

namespace App\Models;

class IdGenerator
{
    private const CHARACTERS = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-_';
    private const ID_LENGTH = 12;

    public function generateId(): string
    {
        $result = '';

        for ($i = 0; $i < self::ID_LENGTH; $i++) {
            $result .= self::CHARACTERS[mt_rand(0, strlen(self::CHARACTERS) - 1)];
        }

        return $result;
    }
}
