<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Http\Response;

class WelcomeTest extends TestCase
{
    /**
     * @test
     */
    public function welcome_should_return_200()
    {
        $response = $this->getJson('/');

        $this->assertEquals($response->getStatusCode(), Response::HTTP_OK);
    }
}
