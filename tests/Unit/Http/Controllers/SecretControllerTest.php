<?php

namespace Tests\Unit\Http\Controllers;

use Carbon\Carbon;
use Tests\TestCase;
use Illuminate\Support\Str;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Cache;

class SecretControllerTest extends TestCase
{
    /**
     * @test
     */
    public function store_should_return_the_correct_response()
    {
        $payload = 'This is the test payload';

        $response = $this->postJson('api/secret', [
            'payload' => $payload
        ]);

        $responseData = json_decode($response->getContent(), true);

        $response->assertStatus(Response::HTTP_CREATED);
        $this->assertTrue(array_key_exists('id', $responseData));
        $this->assertTrue(array_key_exists('url', $responseData));
        $this->assertTrue(array_key_exists('expires_at', $responseData));
    }

    /**
     * @test
     */
    public function store_should_encrypt_the_payload()
    {
        $payload = 'secret payload';

        $response = $this->postJson('api/secret', [
            'payload' => $payload
        ]);

        $responseData = json_decode($response->getContent(), true);

        $encryptedPayload = Cache::get($responseData['id']);
        $this->assertNotEquals($payload, $encryptedPayload);
    }

    /**
     * @test
     */
    public function store_should_store_the_payload_for_an_hour()
    {
        $payload = 'secret payload';

        $response = $this->postJson('api/secret', [
            'payload' => $payload
        ]);

        $responseData = json_decode($response->getContent(), true);

        $this->assertNotNull(Cache::get($responseData['id']));

        Carbon::setTestNow(now()->addHour());

        $this->assertNotNull(Cache::get($responseData['id']));

        Carbon::setTestNow(now()->addHour()->addSecond());

        $this->assertNull(Cache::get($responseData['id']));
    }

    /**
     * @test
     */
    public function store_should_return_422_when_no_payload_is_provided()
    {
        $response = $this->postJson('api/secret', []);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    /**
     * @test
     */
    public function store_should_return_422_when_the_payload_is_too_long()
    {
        $response = $this->postJson('api/secret', [
            'payload' => Str::random(5001),
        ]);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    /**
     * @test
     */
    public function show_should_return_the_decrypted_payload()
    {
        $payload = 'secret payload';

        $response = $this->postJson('api/secret', [
            'payload' => $payload
        ]);
        $responseData = json_decode($response->getContent(), true);

        $response = $this->get("/s/{$responseData['id']}");

        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewHas('secret', $payload);
    }

    /**
     * @test
     */
    public function show_should_remove_the_secret_from_the_cache_when_the_secret_is_viewed()
    {
        $payload = 'secret payload';

        $response = $this->postJson('api/secret', [
            'payload' => $payload
        ]);
        $responseData = json_decode($response->getContent(), true);

        $this->assertNotNull(Cache::get($responseData['id']));

        $this->get("/s/{$responseData['id']}");

        $this->assertNull(Cache::get($responseData['id']));
    }

    /**
     * @test
     */
    public function show_should_return_404_if_the_secret_is_not_found()
    {
        $response = $this->get("/s/blerg");

        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    /**
     * @test
     */
    public function show_should_return_404_if_the_user_is_a_bot()
    {
        $payload = 'secret payload';

        $response = $this->postJson('api/secret', [
            'payload' => $payload
        ]);
        $responseData = json_decode($response->getContent(), true);

        $this->assertNotNull(Cache::get($responseData['id']));

        $response = $this->get("/s/{$responseData['id']}", [
            'HTTP_USER-AGENT' => 'curl/7.54.0'
        ]);

        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }
}
