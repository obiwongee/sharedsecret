<?php

namespace Tests\Unit\Models;

use Tests\TestCase;
use App\Models\IdGenerator;

class IdGeneratorTest extends TestCase
{
    /**
     * @test
     */
    public function generateId_returns_an_id()
    {
        $generator = resolve(IdGenerator::class);

        $id = $generator->generateId();

        $this->assertIsString($id);
        $this->assertSame(12, strlen($id));
    }
}
