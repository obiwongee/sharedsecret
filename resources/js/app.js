require('./bootstrap');

$(document).ready(function() {
    $('.share').on('click', function() {
        let payload = $('.payload').val().trim();

        if (payload == '') {
            alert('Payload cannot be empty');
        } else if (payload.length > 5000) {
            alert('Payload is too long');
        } else {
            submitSecret();
        }
    });

    $('.secret-url').on('click', function() {
        $(this).focus();
        $(this).select();

        document.execCommand("copy");

        toggleAlert("URL copied to clipboard");
    });

    $('.copy-url-button').on('click', function() {
        let element = $('.secret-url');

        element.focus();
        element.select();

        document.execCommand("copy");

        toggleAlert("URL copied to clipboard");
    });

    let hasClickedSecret = false;
    $('.revealed-payload').on('click', function() {
        if (!hasClickedSecret) {
            hasClickedSecret = true;

            $(this).focus();
            $(this).select();

            document.execCommand("copy");

            toggleAlert("Secret copied to clipboard");
        }
    });

    $('.copy-secret-button').on('click', function() {
        hasClickedSecret = true;

        let element = $('.revealed-payload');

        element.focus();
        element.select();

        document.execCommand("copy");

        toggleAlert("Secret copied to clipboard");
    });
})

function submitSecret()
{
    $(".payload").prop("readonly", true);

    $.ajax({
        method: 'POST',
        url: '/api/secret',
        data: {
            payload: $('.payload').val()
        },
        success: function(res) {
            let expiresAt = new Date(res.expires_at);

            let hours = ('0' + expiresAt.getHours()).slice(-2);
            let minutes = ('0' + expiresAt.getMinutes()).slice(-2);
            let expiresString = expiresAt.toDateString() + ' ' + hours + ':' + minutes;

            $('.secret-url').val(res.url);
            $('.expires-at').html(expiresString);

            $('.the-secret').remove();
            $('.the-result').fadeIn();
        },
        error: function(err) {
            alert("OOPS! Something wen't wrong.");

            $(".payload").prop("readonly", false);
        }
    });
}

function toggleAlert(message)
{
    $('.alert').html(message);
    $(".alert").fadeTo(2000, 500).fadeOut(500);
}

