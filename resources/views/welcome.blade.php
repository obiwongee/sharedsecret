<html>
<head>
    <title>Shared Secret</title>

    @include('common.includes')
</head>
<body>
<div class="container">
    @include('header')

    <div class="row the-secret">
        <form id="secret-form">
            <div class="form-row">
                <div class="col d-flex justify-content-center">
                    <div class="secret-container">
                        <textarea name="payload" class="form-control payload" rows="10"></textarea>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col d-flex justify-content-center">
                    <div>
                        <p class="text-center">Your secret is <span class="highlight">encrypted</span> when stored and will <span class="highlight">expire</span> after <span class="highlight">an hour</span> or until it is <span class="highlight">viewed once.</span></p>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col d-flex justify-content-center">
                    <div class="p-2">
                        <input class="share btn btn-primary" type="button" value="Lock it up!"/>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <div class="row the-result secret-url-container">
        <div class="col align-items-center d-flex justify-content-center">
            <div class="p-2">
                <input class='secret-url' type="text" value="blarg" readonly/>
            </div>
            <div class="p-2">
                <input class="copy-url-button btn btn-primary" type="button" value="Copy"/>
            </div>
        </div>
    </div>
    <div class="row the-result">
        <div class="col text-center">
            <p>Your secret is safe 🔒</p>
            <p>It will be stored until <span class="expires-at highlight"></span> or until the URL is viewed once.</p>
        </div>
    </div>

    @include('footer')
</div>
</body>
</html>
