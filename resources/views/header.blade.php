<div class="alert alert-primary alert-fixed" role="alert"></div>

<div class="row" style="margin-top: 40px;">
    <div class="col-lg-12">
        <h1 class="text-center"><a class='header-link' href="{{ config('app.url') }}">Shared Secret</a></h1>
        <p class="text-center"><span class='italic'>Stop sharing your secrets on Slack</span> 🤦</p>
    </div>
</div>
