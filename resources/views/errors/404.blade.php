<html>
<head>
    <title>Shared Secret</title>

    @include('common.includes')

    <style>
        .vertical-center {
            min-height: 50%;  /* Fallback for browsers do NOT support vh unit */
            min-height: 50vh; /* These two lines are counted as one :-)       */

            display: flex;
            align-items: center;
        }

        .http-error-code {
            font-size: 100px;
        }
    </style>
</head>
<body>
<div class="container">
    @include('header')
</div>
<div class="jumbotron vertical-center">
    <div class="container text-center">
        <h1 class="http-error-code">404</h1>
        <p>We couldn't find what you were looking for</p>
    </div>
</div>
</body>
</html>
