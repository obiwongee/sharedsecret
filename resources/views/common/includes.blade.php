<meta property="og:title" content="Shared Secret" />
<meta property="og:type" content="website" />
<meta property="og:url" content="{{ config('app.url') }}" />
<meta property="og:description" content="Stop sharing secrets on Slack. Shared Secret is a safe and secure way to share a secret quickly." />
<meta property="og:image" content="{{ config('app.url') . '/images/lock-512.jpg' }}" />

<meta name="viewport" content="width=device-width, initial-scale=1">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>

<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@600&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css2?family=Baloo+Tammudu+2&family=Source+Sans+Pro:wght@600&display=swap" rel="stylesheet">

<link href="{{ mix('css/app.css') }}" rel="stylesheet"/>

<link rel="apple-touch-icon" sizes="180x180" href="/favico/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="/favico/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="/favico/favicon-16x16.png">
<link rel="manifest" href="/favico/site.webmanifest">
<link rel="mask-icon" href="/favico/safari-pinned-tab.svg" color="#5bbad5">
<meta name="msapplication-TileColor" content="#da532c">
<meta name="theme-color" content="#ffffff">

<script src="{{ mix('js/app.js') }}" type="text/javascript"></script>


