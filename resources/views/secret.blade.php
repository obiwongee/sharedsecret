<html>
<head>
    <title>Shared Secret</title>

    @include('common.includes')
</head>
<body>
<div class="container">
    @include('header')

    <div class="row">
        <div class="col d-flex justify-content-center">
            <div class="secret-container">
                <textarea class="form-control revealed-payload" rows="10" readonly>{{ $secret }}</textarea>
            </div>
        </div>
    </div>

    <div class="row mb-3">
        <div class="col d-flex justify-content-center">
            <div>
                <input class="copy-secret-button btn btn-primary" type="button" value="Copy to clipboard"/>
            </div>
        </div>
    </div>

    @include('footer')
</div>
</body>
</html>
